using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public float velocity  = 5;
    public float jump = 20;
    public GameObject rightBullet;
    public GameObject leftBullet;
    public bool escalera = false;
    public bool piso = false;
    public bool planeando = false;
    public float tiempoAire = 0;
    private GameController _game;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (piso)
        {
            animator.SetInteger("Estado", 0);
        }else
        {
            if (rb.velocity.y < 0 && escalera == false)
            {
                tiempoAire += Time.deltaTime;
            }
        }
        planeando = false;
        rb.velocity = new Vector2(0, rb.velocity.y); 

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocity, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocity, rb.velocity.y); 
            sr.flipX = true; 
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
            
            animator.SetInteger("Estado",2); 
        }   
        if (Input.GetKey(KeyCode.C))
        {
            animator.SetInteger("Estado",5);
            planeando = true;

        } 
        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetInteger("Estado",3); 
        } 
        if (Input.GetKeyUp(KeyCode.X))
        {
            animator.SetInteger("Estado",4); 
            var bullet = sr.flipX ? leftBullet : rightBullet;
            var position = new Vector2(transform.position.x, transform.position.y);
            var rotation = rightBullet.transform.rotation;
            Instantiate(bullet, position, rotation);
            
        }
        if (Input.GetKey(KeyCode.UpArrow) && escalera)
        {
            animator.SetInteger("Estado", 6);
            rb.velocity = new Vector2(rb.velocity.x, 3); 
        }
        if (Input.GetKey(KeyCode.DownArrow) && escalera)
        {
            animator.SetInteger("Estado", 6);
            rb.velocity = new Vector2(rb.velocity.x, -3); 
        }
        if (Input.GetKeyUp(KeyCode.UpArrow) && escalera)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0); 
        }
        if (Input.GetKeyUp(KeyCode.DownArrow) && escalera)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0); 
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Escalera"))
        {
            escalera = true;
            rb.gravityScale = 0;
        }
        
        
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Escalera"))
        {
            escalera = false;
            rb.gravityScale = 6;
        }
        
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            piso = true;
            Debug.Log(tiempoAire);
            if (tiempoAire>=0.6 && planeando == false)
            {
                Destroy(this.gameObject);
            }
            tiempoAire=0;

        }
         if (collision.gameObject.CompareTag("Enemigo"))
        {
            
            _game.LoseLife();
            if (_game.GetLifes()==0)
            {
                Destroy(this.gameObject);
            }
            
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            piso = false;

        }
    }

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerator : MonoBehaviour
{
    
    public GameObject zombie;
    public float tiempo = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempo += Time.deltaTime;
        if (tiempo >= 4)
        {
            var position = new Vector2(transform.position.x, transform.position.y);
            var rotation = zombie.transform.rotation;
            Instantiate(zombie, position, rotation);
            tiempo=0;
        }
        
    }
}
